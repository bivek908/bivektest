<?php
// HTTP
define('HTTP_SERVER', 'https://petshopindia.com/admin/');
define('HTTP_CATALOG', 'https://petshopindia.com/');

// HTTPS
define('HTTPS_SERVER', 'https://petshopindia.com/admin/');
define('HTTPS_CATALOG', 'https://petshopindia.com/');

// DIR
define('DIR_APPLICATION', '/home3/petsh6fb/public_html/admin/');
define('DIR_SYSTEM', '/home3/petsh6fb/public_html/system/');
define('DIR_LANGUAGE', '/home3/petsh6fb/public_html/admin/language/');
define('DIR_TEMPLATE', '/home3/petsh6fb/public_html/admin/view/template/');
define('DIR_CONFIG', '/home3/petsh6fb/public_html/system/config/');
define('DIR_IMAGE', '/home3/petsh6fb/public_html/image/');
define('DIR_CACHE', '/home3/petsh6fb/public_html/system/cache/');
define('DIR_DOWNLOAD', '/home3/petsh6fb/public_html/system/download/');
define('DIR_UPLOAD', '/home3/petsh6fb/public_html/system/upload/');
define('DIR_LOGS', '/home3/petsh6fb/public_html/system/logs/');
define('DIR_MODIFICATION', '/home3/petsh6fb/public_html/system/modification/');
define('DIR_CATALOG', '/home3/petsh6fb/public_html/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'petsh6fb_ppitshop');
define('DB_PASSWORD', 'tj.owr]vFT)~');
define('DB_DATABASE', 'petsh6fb_ppitshop');
define('DB_PORT', '3306');
define('DB_PREFIX', 'lp_');
